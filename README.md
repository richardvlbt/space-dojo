﻿# Space Dojo

## Introductie
Voor onze eerste game gaan we een oneindige space shooter maken.
We gebruiken hier assets van o.a. `kenney.nl` voor. Hieronder zullen alle benodigdheden in een lijstje worden getoond.
Als je wat wil afwisselen kan je natuurlijk ook andere grafische elementen gebruiken of zelf wat maken!

### Benodigdheden:
- Kenney simple space pack: https://kenney.nl/assets/simple-space
- Kenney sci-fi sounds: https://kenney.nl/assets/sci-fi-sounds

### Installatie Unity en Visual Studio Code
Zorg er voor dat je de nieuwste versie van Unity 2021 LTS hebt geïnstalleerd.
Op het moment van schrijven is dat versie 2021.3.4f1.
Zorg er ook voor dat je Visual Studio Code (of vergelijkbaar) hebt geïnstalleerd, zodat je ook makkelijker code kunt gaan schrijven.

Mocht je hier niet uitkomen, vraag hulp aan 1 van de instructeurs.

### Opzetten van het project
Als Unity is geïnstalleerd kan je beginnen met het aanmaken van een nieuw project.
Kies als naam voor het project Space Dojo (of verzin zelf iets leuks!).
Als template kies je dan voor `2D URP`. Creëer het project en wanneer het opgestart is kun je gaan beginnen!

### Importeren van assets
Om een spel te kunnen maken hebben we ook assets nodig. Assets is een verzamelnaam voor grafische elementen, geluiden, muziek etc.
Zorg er voor dat je alle assets die in het lijstje hierboven staan hebt gedownload.
Maak daarna in het Projectoverzicht een aantal verschillende mappen aan. Deze staan hieronder opgenoemd.

- Graphics
- Prefabs
- Scripts
- Sounds

Er is al een `Scenes` map aangemaakt door Unity zelf waar straks je levels in komen te staan.

## Player beweging
Als je alles hebt geïmporteerd en klaar hebt gezet ben je klaar om te beginnen.
Kies 1 van de schepen uit als je player model en sleep dit plaatje dan vervolgens de `Scene view` in.
Wanneer je dit doet wordt er een nieuw object aangemaakt in de hiërarchie van je spel.
Het overzicht van objecten staat standaard aan de linkerkant van je scherm.
Wanneer je je `Player` hebt geselecteerd komen aan de rechterkant van je scherm meer details te staan, hier kan je nieuwe componenten toevoegen, zoals een script voor beweging.

### Script toevoegen
Om een script toe te voegen om je ruimteschip te kunnen laten bewegen, klik je op `Add component`.
Onderin deze lijst vindt je `New script`, klik hier op. Typ dan daarna `Player` in en klik op `Create and add`.
Het script komt dan automatisch terecht in je `Assets` folder. Versleep deze in het projectoverzicht naar je `Scripts` map.
Als je dan vervolgens dubbelklikt op het script, wordt deze geopend in Visual Studio Code.

Het script dat je dan opent ziet er standaard zo uit:

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
```

Het belangrijkste om nu naar te kijken zijn de code blokken die worden toegevoegd (methodes).
Dit zijn `Start` en `Update`. Unity maakt gebruik van deze methodes om je code toe te passen op het object.

#### Start
De `Start` methode wordt maar eenmalig uitgevoerd door Unity.
Hier kun je dus code zetten die alleen voor het opzetten van het object hoeft te gebeuren.
Een voorbeeld hiervan zou ---TODO--- kunnen zijn.

#### Update
De `Update` methode wordt door Unity elk frame aangeroepen (deze blijft zich herhalen als je game aanstaat).
In deze methode zal je bijvoorbeeld de player besturing moeten toevoegen.

### Toevoegen code voor simpele beweging
Om je ruimteschip te laten bewegen moeten we een aantal dingen doen.
Allereerst moeten we natuurlijk aan Unity laten weten dat er iets moet gebeuren als er op een knop gebeurt.
We moeten de input van de gebruiker registreren en daar iets mee doen.

We beginnen met het registeren van de pijltjes toetsen (of AWSD toetsen).
Unity heeft hier handige functies voor, namelijk:
`Input.GetAxis("Horizontal")` en
`Input.GetAxis("Vertical")`
Hiermee kan je de links en rechts beweging en de boven en beneden beweging registeren.

Als je deze waarden hebt opgehaald, kan je dat bij de huidige positie van de speler optellen waardoor je ruimteschip kan bewegen.
De huidige locatie van de speler kan je ophalen met `transform.position`.

Het stukje code voor het bewegen van de speler ziet er dan zo uit:

```c#
float xPosition = Input.GetAxis("Horizontal") * Time.deltaTime * 5;
float yPosition = Input.GetAxis("Vertical") * Time.deltaTime * 5;

transform.position = new Vector2(transform.position.x + xPosition, transform.position.y + yPosition);
```


