using System;
using UnityEditor.Search;
using UnityEngine;
using UnityEngine.UIElements;

public class Player : MonoBehaviour
{
    [SerializeField] private int forwardMovementSpeed = 40;
    [SerializeField] private int backwardsMovementSpeed = 20;
    [SerializeField] private int maximumSpeed = 200;
    [SerializeField] private int projectileSpeed = 10;
    [SerializeField] private Projectile projectile;
    [SerializeField] private GameObject firePosition;
    [SerializeField] private float screenPadding = 0.5f;

    private Camera mainCamera;
    private Rigidbody2D cachedRigidbody;
    float xMin, xMax, yMin, yMax;

    private void Start()
    {
        cachedRigidbody = GetComponent<Rigidbody2D>();
        mainCamera = Camera.main;
        SetMovementBoundaries();
    }

    void Update()
    {
        Move();
        Rotate();
        Shoot();
    }

    void Move()
    {
        if (Input.GetKey("up") || Input.GetKey(KeyCode.W))
            cachedRigidbody.AddForce(transform.up * (Time.deltaTime * forwardMovementSpeed));
        
        if (Input.GetKey("down") || Input.GetKey(KeyCode.S))
            cachedRigidbody.AddForce(transform.up * (Time.deltaTime * -backwardsMovementSpeed));
        
        cachedRigidbody.velocity = Vector2.ClampMagnitude(cachedRigidbody.velocity, maximumSpeed);

        transform.position = new Vector2(
            Mathf.Clamp(transform.position.x, xMin, xMax),
            Mathf.Clamp(transform.position.y, yMin, yMax)
        );
    }

    void Rotate()
    {
        Vector2 mousePosition = GetMousePosition();
        Vector2 direction = (mousePosition - (Vector2) transform.position).normalized;
        transform.up = direction;
    }

    void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Projectile spawnedProjectile = Instantiate(projectile, firePosition.transform.position, Quaternion.identity);
            spawnedProjectile.GetComponent<Rigidbody2D>().velocity = transform.up * projectileSpeed;
        }
    }

    Vector2 GetMousePosition()
    {
        return mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    void SetMovementBoundaries()
    {
        xMin = mainCamera.ViewportToWorldPoint(new Vector3(0,0,0)).x + screenPadding;
        xMax = mainCamera.ViewportToWorldPoint(new Vector3(1,0,0)).x - screenPadding;

        yMin = mainCamera.ViewportToWorldPoint(new Vector3(0,0,0)).y + screenPadding;
        yMax = mainCamera.ViewportToWorldPoint(new Vector3(0,1,0)).y - screenPadding;

    }
}
